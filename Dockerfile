FROM php:5-fpm

RUN apt-get update && \
    apt-get install -y libmcrypt-dev libpq-dev netcat && \
    apt-get install -y phpunit && \
    rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install \
        mcrypt \
        bcmath \
        mbstring \
        exif \
        zip \
        opcache \
        pdo pdo_mysql

RUN echo 'UTC' > /etc/timezone

RUN yes | pecl install xdebug-beta \
        && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
        && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
        && echo "xdebug.remote_autostart=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
        && echo "xdebug.remote_connect_back=on" >> /usr/local/etc/php/conf.d/xdebug.ini

RUN echo 'date.timezone="UTC"' >> /usr/local/etc/php/php.ini

COPY support/php/fpm_www.conf /usr/local/etc/php-fpm.d/www.conf
COPY . /srv/

WORKDIR /srv
CMD ["bash", "boot.sh"]
