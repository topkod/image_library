<?php

namespace spec\AppBundle\Service;

use AppBundle\Service\GalleryService;
use AppBundle\Repository\AlbumRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GalleryServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(GalleryService::class);
    }

    public function let(
        Paginator $paginator,
        AlbumRepository $albumRepository
    ) {
        $this->beConstructedWith($paginator, $albumRepository);
    }
}
