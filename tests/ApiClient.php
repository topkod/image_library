<?php

namespace Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ApiClient
{
    /**
     * @var Client
     */
    private $client;


    public static function create()
    {
        $obj = new self();

        $obj->client = new Client([
            'base_uri' => 'http://front',
        ]);

        return $obj;
    }

    /**
     * @param $method
     * @param $uri
     * @param $options
     *
     * @return JsonResponse
     */
    public function json($method, $uri = null, $options = [])
    {
        try {
            if ($options && !array_key_exists('json', $options)) {
                $options = [
                    'json' => $options,
                ];
            }

            $response = $this->client->request($method, $uri, $options);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        return new JsonResponse($response);
    }
}
