<?php

namespace Tests;

use Psr\Http\Message\ResponseInterface;

class JsonResponse
{
    private $response;
    private $data;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getData()
    {
        if (!$this->data) {
            $this->data = \json_decode($this->response->getBody()->getContents(), true);
        }

        return $this->data;
    }

    public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }
}
