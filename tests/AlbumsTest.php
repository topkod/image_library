<?php

namespace Tests;

class AlbumsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ApiClient
     */
    public static $client;

    public static function setUpBeforeClass()
    {
        self::$client = ApiClient::create();
    }

    public function testGetAction()
    {
        $response = self::$client->json('GET', '/api/v1/album');

        $this->assertEquals(200, $response->getStatusCode(), 'Status code does not matched');

        $this->assertArrayHasKey('items', $response->getData());
        $this->assertNotEmpty($response->getData()['items']);
        $album = array_shift($response->getData()['items']);
        $this->assertArrayHasKey('images', $album);
    }

    public function testShowAction()
    {
        $response = self::$client->json('GET', '/api/v1/album/1');

        $this->assertEquals(200, $response->getStatusCode(), 'Status code does not matched');

        $album = $response->getData();
        $this->assertNotEmpty($album);
        $this->assertEquals(1,$album['id']);
        $this->assertArrayHasKey('images', $album);
    }
}