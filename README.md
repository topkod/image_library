### What is this repository contains? ###

* Sample image library API application

### How do I get set up? ###

* Install Docker https://docs.docker.com/engine/installation/
* Go to image_library directory and run 

```
#!bash

docker-compose build
```

Docker containers images will be created

* After the images are ready run next command to install vendors

```
#!bash

./support/shortcuts/composer install

```
It's a simple shortcut to run the composer in a container also

* After installation  of vendors you should to "up" docker containers using docker-compose up command with -d parameters. Using -d parameter your containers will started in detached mode.

```
#!bash

#!docker-compose up -d

```

* That's all ! Then your can access the image library application at 8081 port of the your docker machine if you use it or by ip of your machine.

### Some helpful information ###

* You can use the Symfony console using the command "./support/shortcuts/console" that also will be executed in container.
* I provide a few simple test that can be ran by command "./support/scripts/run_tests" 
* I use Docker because it more preferable way to install all needs stuff if you develops many application and didn't want to install all environment directly on your machine.  
* Fill free to contact me via email or Skype for any question