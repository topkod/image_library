<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class AlbumRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAlbumsQuery()
    {
        $qb = $this->createQueryBuilder(Album::ALIAS);
        return $qb->getQuery();
    }
    
    public function getMetadata()
    {
        return $this->getClassMetadata();
    }

    public function buildLastImagesForAlbumsNativeQuery(
        array $albums,
        ResultSetMapping $rsm,
        $limit = 10
    )
    {
        $albumClassMetadata = $this->getClassMetadata();
        $albumAlias = Album::ALIAS;

        $imageClassMetadata = $this->_em->getRepository(Image::class)->getClassMetadata();
        $imageAlias = Image::ALIAS;

        $albumColumns = array_map(function ($column) use ($albumAlias) {
            return $albumAlias.'.'.$column.' as '.$albumAlias.'_'.$column;
        }, $albumClassMetadata->getColumnNames());

        $imagesColumns = array_map(function ($column) use ($imageAlias) {
            return $imageAlias.'.'.$column.' as '.$imageAlias.'_'.$column;
        }, $imageClassMetadata->getColumnNames());

        $selectColumns = implode(',',$albumColumns).','.implode(',', $imagesColumns);

        $queryParts = [];
        foreach ($albums as $album) {
            $albumId = ($album instanceof Album)?$album->getId():$album['id'];

            $queryPart = "(SELECT {$selectColumns} "
                ."FROM {$imageClassMetadata->getTableName()} {$imageAlias} ".
                "RIGHT JOIN {$albumClassMetadata->getTableName()} {$albumAlias} ".
                    "ON {$albumAlias}.id = {$imageAlias}.album_id ".
                "WHERE {$albumAlias}.id = {$albumId} ".
                "LIMIT {$limit}".
                ")";
            $queryParts[] = $queryPart;
        }
        $query = implode(" UNION ",$queryParts);
        
        return $this->_em->createNativeQuery($query, $rsm);
    }
}