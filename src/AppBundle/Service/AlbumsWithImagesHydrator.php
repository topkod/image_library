<?php

namespace AppBundle\Service;


use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use AppBundle\Repository\AlbumRepository;
use AppBundle\Repository\ImageRepository;
use Doctrine\ORM\Query\ResultSetMapping;
class AlbumsWithImagesHydrator
{
    /**
     * @var \AppBundle\Repository\AlbumRepository
     */
    protected $albumRepository;

    /**
     * @var \AppBundle\Repository\ImageRepository
     */
    protected $imageRepository;

    /**
     * AlbumsWithImagesHydrator constructor.
     * @param AlbumRepository $albumRepository
     * @param ImageRepository $imageRepository
     */
    public function __construct(AlbumRepository $albumRepository, ImageRepository $imageRepository)
    {
        $this->albumRepository = $albumRepository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @param array $albums
     * @param int $imagesLimit
     * @return array
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function hydrate(array $albums, $imagesLimit = 10)
    {
        $albumClassMetadata = $this->albumRepository->getMetadata();

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Album::class,Album::ALIAS);

        foreach ($albumClassMetadata->getColumnNames() as $columnName) {
            $rsm->addFieldResult(
                Album::ALIAS,
                Album::ALIAS.'_'.$columnName,
                $albumClassMetadata->getFieldForColumn($columnName)
            );
        }
        
        $rsm->addJoinedEntityResult(
            Image::class,
            Image::ALIAS,
            Album::ALIAS,
            Album::IMAGES_FIELD_NAME
        );

        $imageClassMetadata = $this->imageRepository->getMetadata();

        foreach ($imageClassMetadata->getColumnNames() as $columnName) {
            $rsm->addFieldResult(
                Image::ALIAS,
                Image::ALIAS.'_'.$columnName,
                $imageClassMetadata->getFieldForColumn($columnName)
            );
        }
        
        $query = $this->albumRepository->buildLastImagesForAlbumsNativeQuery(
            $albums,
            $rsm,
            $imagesLimit
        );

        return $query->getResult();
    }
}