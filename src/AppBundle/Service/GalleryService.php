<?php

namespace AppBundle\Service;

use AppBundle\Repository\AlbumRepository;
use AppBundle\Subscriber\HydrateAlbumsWithImagesSubscriber;
use Knp\Component\Pager\PaginatorInterface;

class GalleryService
{
    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var AlbumRepository
     */
    protected $albumRepository;

    /**
     * GalleryService constructor.
     * @param PaginatorInterface $paginator
     * @param AlbumRepository $albumRepository
     */
    public function __construct(PaginatorInterface $paginator, AlbumRepository $albumRepository)
    {
        $this->paginator = $paginator;
        $this->albumRepository = $albumRepository;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getAlbumsWithPreviewPaginated($page = 1, $limit = 10)
    {
        return $this->paginator->paginate(
            $this->albumRepository->getAlbumsQuery(),
            $page,
            $limit,
            [HydrateAlbumsWithImagesSubscriber::HYDRATE_ALBUMS_OPTION_NAME => true,]
        );
    }
}