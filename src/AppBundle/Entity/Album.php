<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Entity
 * @package AppBundle\Entity
 * @ORM\Table(name="albums")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlbumRepository")
 */
class Album
{
    const ALIAS = 'album';
    const IMAGES_FIELD_NAME = 'images';
    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", name="author")
     */
    private $author;

    /**
     * @var ArrayCollection[]|Image
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="album")
     */
    private $images;

    /**
     * Entity constructor.
     */
    private function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @param $name
     * @param $author
     * @return Entity
     */
    public static function create($name, $author)
    {
        $obj = new self();
        $obj->name = $name;
        $obj->author = $author;

        return $obj;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function getImages()
    {
        return $this->images;
    }
}