<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Entity
 * @package AppBundle\Entity
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image
{
    const ALIAS = 'image';
    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", name="filename")
     */
    private $filename;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true, name="exif_info")
     */
    private $exifInfo;

    /**
     * @var Album
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="features", cascade={"persist"})
     */
    private $album;

    /**
     * Entity constructor.
     */
    private function __construct(){}

    /**
     * @param $name
     * @param $filename
     * @param array $exifInfo
     * @return Image
     */
    public static function create($name, $filename, array $exifInfo = [])
    {
        $obj = new self();
        $obj->name = $name;
        $obj->filename = $filename;
        $obj->exifInfo = $exifInfo;

        return $obj;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return array
     */
    public function getExifInfo()
    {
        return $this->exifInfo;
    }

    /**
     * @param Album $album
     * @return $this
     */
    public function linkToAlbum(Album $album)
    {
        $this->album = $album;
        return $this;
    }
}