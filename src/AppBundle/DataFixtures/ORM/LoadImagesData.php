<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class LoadActionData extends AbstractFixture implements OrderedFixtureInterface
{
    private $fileDir = '/web/uploads';
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $images = $this->getFilesArray();
        $faker = Factory::create();
        for ($i = 0; $i < 5; ++$i) {
            if($i==0) {
                $imagesCount = 5;
            } else {
                $imagesCount = rand(21,count($images));
            }
            $imagesForAlbum = $faker->randomElements($images,$imagesCount);
            $album = $this->getReference("album-{$i}");
            /**
             * @var $imageForAlbum SplFileInfo
             */
            foreach($imagesForAlbum as $imageForAlbum) {
                @$exifData = exif_read_data($imageForAlbum->getPathname());
                $image = Image::create(
                    $faker->name,
                    $imageForAlbum->getFilename(),
                    $exifData?:[]
                );
                $image->linkToAlbum($album);
                $manager->persist($image);
            }
        }
        $manager->flush();
    }

    protected function getFilesArray()
    {
        $files = [];
        $finder = Finder::create();
        $finder->files()->in($this->fileDir);
        foreach ($finder as $file) {
            $files[] = $file;
        }
        return $files;
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }
}
