<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Finder\Finder;

class LoadAlbumsData extends AbstractFixture implements OrderedFixtureInterface
{
    private $albums = [
        'Nature',
        'Family',
        'Pets',
        'Home',
        'Other'
    ];
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        foreach($this->albums as $i=>$album) {
            $album = Album::create($album,$faker->name);
            $manager->persist($album);
            $this->addReference("album-{$i}", $album);
        }
        $manager->flush();
    }
    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }
}
