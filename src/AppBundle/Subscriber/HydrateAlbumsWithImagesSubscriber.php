<?php

namespace AppBundle\Subscriber;

use AppBundle\Service\AlbumsWithImagesHydrator;
use Knp\Component\Pager\Event\AfterEvent;
use Knp\Component\Pager\Event\ItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class HydrateAlbumsWithImagesSubscriber implements EventSubscriberInterface
{
    const HYDRATE_ALBUMS_OPTION_NAME = 'hydrateAlbumsWithImages';
    const DEFAULT_PREVIEWS_COUNT = 10;

    /**
     * @var AlbumsWithImagesHydrator
     */
    protected $albumsWithImagesHydrator;

    public function __construct(AlbumsWithImagesHydrator $albumsWithImagesHydrator)
    {
        $this->albumsWithImagesHydrator = $albumsWithImagesHydrator;
    }

    /**
     * @param AfterEvent|ItemsEvent $event
     */
    public function hydrate(AfterEvent $event)
    {
        $hydrate = $event->getPaginationView()
            ->getPaginatorOption(self::HYDRATE_ALBUMS_OPTION_NAME);
        if($hydrate) {
            $pagination = $event->getPaginationView();
            $items = $this->albumsWithImagesHydrator
                ->hydrate($pagination->getItems(),self::DEFAULT_PREVIEWS_COUNT);
            $pagination->setItems($items);
            $event->stopPropagation();
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.after' => array('hydrate', 1)
        );
    }
}