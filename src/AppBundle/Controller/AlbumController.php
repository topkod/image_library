<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use FOS\RestBundle\FOSRestBundle;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends FOSRestBundle
{
    /**
     * @Rest\View()
     * @Method({"GET"})
     * @Route("/api/v1/album", name="albums_list")
     * @param Request $request
     * @return mixed
     */
    public function getAlbumsWithPreviewAction(Request $request)
    {
        $pagination = $this->container->get('app_gallery_service')
            ->getAlbumsWithPreviewPaginated(
                $request->query->get('page',1),
                $request->query->get('limit',10)
            );
        return [
            'pagination' => $pagination->getPaginationData(),
            'items' => iterator_to_array($pagination),
        ];
    }

    /**
     * @param Album $album
     * @Route("/api/v1/album/{id}", name="album_info")
     * @ParamConverter("post", class="AppBundle:Album")
     * @Method({"GET"})
     * @return Album
     */
    public function showAlbumAction(Album $album)
    {
        return $album;
    }
}