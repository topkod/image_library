#!/usr/bin/env bash

# Disable xdebug in production environment
xdebug_config=/usr/local/etc/php/conf.d/xdebug.ini
if [ -f $xdebug_config ] && [ "$SYMFONY_ENV" == "prod" ]; then
    rm $xdebug_config
fi

# Prepare application
bin/console cache:clear
bin/console doctrine:database:drop --force
bin/console doctrine:database:create
bin/console doctrine:migration:migrate -n
bin/console doctrine:fixtures:load -n

php-fpm --allow-to-run-as-root
